# Esercizio 7

import numpy as np
from matplotlib import pyplot as plt

x=np.linspace(-2*np.pi,2*np.pi,50)
y=np.sin(x)

plt.figure(figsize=(16,8))
plt.plot(x,y, '.--r')
plt.show()
