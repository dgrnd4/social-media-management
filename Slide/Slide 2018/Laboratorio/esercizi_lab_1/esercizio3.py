# Esercizio 3
#
# Si considerino le seguenti liste:

# l1 = [1, 2, 3]
# l2 = [4,5,6]
# l3 = [5,2,6]

# Si combinino un ciclo for, zip e enumerate per ottenere il seguente output:
# 0 -> 10
# 1 -> 9
# 2 -> 15

l1 = [1,2,3]
l2 = [4,5,6]
l3 = [5,2,6]

for idx, val in enumerate(zip(l1,l2,l3)):
  print (idx, " -> ", sum(val))
