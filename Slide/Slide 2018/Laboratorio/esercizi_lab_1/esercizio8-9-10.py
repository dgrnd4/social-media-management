import os
import glob
import numpy as np
from matplotlib import pyplot as plt
from skimage import io as sio

class Dataset:

  def __init__(self, path_to_dataset):
    self.path_to_dataset = path_to_dataset
    classes=os.listdir(path_to_dataset)
    self.paths = dict()

    for c1 in classes:
      current_paths = sorted(glob.glob(os.path.join(path_to_dataset, c1, "*.jpg")))
      self.paths[c1] = current_paths

  def getImagePath(self, c1, idx):
    return self.paths[c1][idx]

  def getClasses(self):
    return sorted(self.paths.keys())
  
  def getNumberOfClasses(self):
    return len(self.paths)
  
  def getClassLength(self, c):
    return len(self.paths[c])
  
  def getLength(self):
    return sum(len(self.paths[c]) for c in self.paths)
  
  def showImage(self, class_name, image_number):
    im = sio.imread(self.getImagePath(class_name, image_number))
    plt.figure()
    plt.imshow(im)
    plt.show()
  
  def restrictToClasses(self, classes):
    new_paths = { c1: self.paths[c1] for c1 in classes }
    # self.paths = new_paths
    return new_paths

d = Dataset("../Caltech101")

# Esercizio 8
#
# Scrivere il codice per mostrare:
# I primi 50 elementi di classes;
# Gli ultimi 10 elementi di classes;
# Gli elementi pari di classes;
# Gli elementi displari di classes;
# Gli elementi di classes in ordine inverso

classes = d.getClasses()
print (classes[0:50])
print (classes[:-10])
print (classes[0::2])
print (classes[1::2])
print (classes[::-1])

# Esercizio 9
#
# Scrivere il codice per mostrare:
# I path delle prime 5 immagini della classe "Faces";
# I path delle prime 3 immagini della quinta classe contenuta in classes;
# La prima metà dei path delle immagini dell'ultima classe contenuta in classes

print ("\n\n")

for x in range(5):
  print (d.getImagePath("Faces", x))

print ("\n\n")

for x in range(3):
  print (d.getImagePath(classes[4], x))
print ("\n\n")

for x in range(int((d.getClassLength(classes[-1]))/2)):
  print (d.getImagePath(classes[-1], x))

# Esercizio 10
# Scrivere il codice per mostrare:
# La terza immagine della classe "Faces";
# L'ultima immagine dell'ultima classe contenuta in classes;
# La penultima immagine della classe "butterfly"

d.showImage("Faces", 3)
d.showImage(classes[-1], d.getClassLength(classes[-1])-1)
d.showImage("butterfly", d.getClassLength("butterfly")-2)
