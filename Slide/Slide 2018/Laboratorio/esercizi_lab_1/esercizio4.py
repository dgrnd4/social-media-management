# Esercizio 4
# Si ripeta l'esercizio 2
# utilizzando la comprensione di dizionari. A tale scopo, sidefinisca prima la lista
#  ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
# Si  costruisca dunque il  dizionario desiderato  utilizzando la  comprensione di dizionari e la funzione enum

mesi = ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']

mesi_ = {}
for idx, val in enumerate(mesi):
  mesi_[val] = idx+1

print (mesi_["Gennaio"])
